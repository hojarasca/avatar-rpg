# Basic moves

## Asses a situation

Básicamente acá la idea es "ver que onda". Intentar
sacar mas información del escenario actual. La jugadora
puede hacer una o mas de las sigueintes preguntas:

- Que puedo usar acá para ___?
- Que o quién es la mayor amenaza?
- Que debería estar buscando?
- Cuál es la mejor manera de entrar salir o atravesar esta situación?
- Que o quién está en mayor peligro?

Cuando se opere basado en la información obtenida las tiradas tienen + 1.
(+1 ongoing)


@startuml asses-a-situation
:Asses a situation;
switch (Tirar un dado **creatividad**)
case ( 0 - 6 )
  :Falla. 0 preguntas;
case ( 7 - 9 ) 
  :Éxito parcial. 1 pregunta;
case ( 10 - 12 )
  :Éxitazo. 2 preguntas;
endswitch
@enduml



## Guide and confort

Esta acción pasa por darle guía o soporte de algún tipo a otro persona (jugable o no).
Pueden ser palabras de aliento, ayuda con un problema o un tesito caliente con un apapacho.

```plantuml
@startuml guide-and-confort
:Guide and confort;
switch (Tirar un dado **Harmony**)
case ( 0 - 6 )
  :Falla. No pasa nada;
  stop
case ( 7 - 12 ) 
  switch (Respuesta del receptor)
    case (acepta 7 - 9)
     :El receptor cura 2 fatiga o una confición.;
     :quién guía puede hacer una pregunta \n y recibir una respuesta sincera.;
    case (acepta 10 - 12)
     :Como lo anterior,
     pero además el guía puede cambiar
     un balanace rel receptor;
    case (rechaza)
     :El receptor inflije una condición \n en el guía;
     :El guía mueve el balance del objetivo en 1;
  endswitch  
endswitch
@enduml
```

## Intimidar

Intentar infundir miedo a alguien. Si la intimifación
es un éxito pasa 1 de 4 cosas:

- El objetivo huye a la fuga o en busca de ayuda.
- El objetivo se aleja pero queda en vigilacnia
- El objetivo cede, pero con condiciones.
- El objetivo ataca, pero fuera de balance. Recibe una condición impuesta por el GM.

```plantuml
@startuml intimidar
:Intimidar;
switch (Tirar dado **Passion**)
case (0-6)
  :La intimidación no funcionó;
case (7-9)
  :El objetivo elije una de las 4 acciones;
case (10 - 12)
  :Quién inició la acción entre las 4 acciones;
endswitch
@enduml
```

## Plead

Básicamente es manguearle cosas a un NPC. Esta acción
no se puede hacer entre jugadoras.

```plantuml
@startuml plead
:Plead;
switch (Tirar **Harmony**)
case (0 - 6)
  :El NPC se niega;
case (7 - 9)
  :El NPC acepta, pero pone algún tipo de condición;
case (10 - 12)
  :El NPC acepta sin condiciones;
endswitch
@enduml
```


## Push your luck

Básicamente esto pasa cuando alguien realiza una acción
que sabe que no tiene por qué salir bien. 

Pushear la suerte **siempre** tiene un costo. El tema
es cuanto es el costo y cuanta es la ganancia.

```plantuml
@startuml push-your-luck
:Push your luck;
switch (Tirar **Harmony**)
case (0 - 6)
  :No hubo suerte. El resultado no es el esperado;
case (7 - 9)
  :Las cosas salen bien pero raspando. Hay consecuencias.;
case (10 - 12)
  :Las cosas salieron bien y te sentís una capa. El costo es bajo.;
endswitch
@enduml
```


## Rely on your skills and training

Básicamente es hacer cosas que saber hacer. Esas en las
cuales se siente que el personaje la tiene clara.


```plantuml
@startuml rely-on-your-training
:Confiar en tu entrenamiento;
switch (Tirar **Focus**)
case (0 - 6)
  :Algo sale mal.;
case (7 - 9)
  switch (GM anuncia algo malo)
    case (gastar 1 fatiga)
        :La acción se realiza sin cosa mala;
    case (no gastar fatiga)
        :La acción se realiza. La cosa mala pasa;
  endswitch
case (10 - 12)
  :Todo sale bien;
endswitch
@enduml
```

## Trick

El personaje engaña de alguna manera algún NPC. Engañar 
puede producir los siguientes efectos.

- Se la cree. De ahora en delante hay un +1 contra ese NPC
(+1 ongoing)
- Reaccionan haciendo alguna boludez. El GM especifica
que hace y que oportunidad produce.
- La siguen. Creen algo incorrecto por un tiempo.


```plantuml
@startuml trick
:Trick;
switch(Tirar **Creativity**)
    case (0 - 6)
    :Algo salió mal;
    case (7 - 9)
    :Se elije una de las 3 opciones;
    case (10 - 12)
    :Se elijen 2 de las opciones;
endswitch
@enduml
```


## Help

Brindar algún tipo de ayuda a otra personaje haciendo algo.

Gastar uno de fatiga para sumar uno al dado.
Se puede hacer después de que la otra persona haya tirado
el dado.



# Balance moves

## Seguir tus principios.

Cunado una acción se justifica con tus principios
podés realizar este movimiento. Esto hace que se reemplace
el dado de lo que sea ibas a tirar por tu balance.

Sin embargo hacer esto cuesta 1 de fatiga.

## Recordarsela a alguien.

Cuando alguien no está realizando acciones a la altura de
sus principios se puede hacer un llamado de atención.
Quién recibe puede ser una Jugadora o un NPC.

```plantuml
@startuml call-someone-out
:Alguien realiza una acción;
:Una jugadora le acusa de no respetar sus principios;
:La jugadora mueve su balance en 1 lejos del centro;
:La nombra un principio del receptor.;
if (La jugadora tira el dado \n usando ese principio del objetivo) then (0-6)
 :El objetivo demanda a la jugadora que viva acorde
 a sus principios;
 split
    :La jugadora marca una condición;
 splitagain
    :La jugadora hace lo que le dicen;
 end split
 stop
else (7 - 12)
  if (6 - 9)
   :El receptor challegea el llamado;
   split
   :La jugadora toma 1 de fatiga;
   splitagain
   :marca 1 condición;
   end split
  endif
endif

split
:El receptor hace lo que se le dice;
split again
:El receptor marca una condición;
end split
stop
@enduml
```

## Rechazar un llamado a tus principios

Cuando un NPC te llama a vivir a la altura de tus 
principios podés intentar rechazar el llamado.

Este es uno de los raros casos en los que en general
se quiere que la tirada salga mal.

```plantuml
@startuml reject-being-called
:Un NPC challengea a una jugadora;
:El NPC nombra un principio de la jugadora;
switch (la jugadora tira con ese principio)
  case (0 - 6)
    :La jugadora se mantuvo fuerte a su impulso inicial;
    split
    :curar 1 fatiga;
    split again
    :curar 1 confición;
    split again
    :mover el balance en 1 
    a elección;
    end split
    stop
  case (7 - 12)
    split
    :La jugadora hace lo que se le dice;
    split again
    :La jugadora marca 1 de fatiga;
    end split
    if (10 - 12) then (si)
        :Las palabras golpearon duro;
        :Avanzar en 1 el balance en la dirección nombrada;
    else (no)
    endif
    stop
endswitch
@enduml
```

## Resistir a que te cambien el balance

Cuando un NPC trata de cambiar tu balance podés
ofrecer resistencia.

Si logras resistir elegís entre las siguientes
3 opciones:

- Sacar una condición o marcar una casilla de crecimiento.
-  Mover tu balance hacia el lado opuesto
- Aprendé acerca de que quiere la otra persona. Si ya lo sabés tenés un +1 onwards con ese NPC.

```plantuml
@startuml resist-balance-shift
:Resistir cambio de balance;
switch (Dado sin modificadores)
  case (0-6)
    :Marcar una condición;
    :El GM mueve 2 veces tu balance;
  case (7-9)
    :Te mantenés firme. Elegí una opción;
  case (10-12)
    :Te mantenés firme. Elegí 2 opciones;
endswitch
@enduml
```

# Perder el balance:

Elegí una de 3:

- Rendite a la oposición.
- Perder control de vos en una manera dañina y/o
destructiva
- Tomar una acción extrema alineada con el principio.
Después huir. Después de un tiempo de reflexión
tu centro de balance se mueve 1. Además se curan
todas tus condiciones y tu fatiga y tu balance
se va al nuevo centro.